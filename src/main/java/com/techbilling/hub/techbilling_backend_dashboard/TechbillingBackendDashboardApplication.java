package com.techbilling.hub.techbilling_backend_dashboard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TechbillingBackendDashboardApplication {

	public static void main(String[] args) {
		SpringApplication.run(TechbillingBackendDashboardApplication.class, args);
	}

}
